from django.http.response import HttpResponse
from django.shortcuts import render
from django.template import loader
from django.views.generic import TemplateView, ListView
from thread.models import Topic


# Create your views here.
def top(request):
    ctx = {'title': 'IT学習ちゃんねる（仮）'}
    return render(request, 'base/top.html', ctx)


class TopView(TemplateView):
    template_name = 'base/top.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['title'] = 'IT学習チャンネル（仮）'
        return ctx


class TopicListView(ListView):
    template_name = 'base/top.html'
    context_object_name = 'topic_list'
    # queryset = Topic.objects.order_by('-created')

    def get_queryset(self):
        query_set = Topic.objects.order_by('-created')
        return query_set
