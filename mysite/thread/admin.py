from django.contrib import admin

from .models import Category, Comment, Topic

# Register your models here.
admin.site.register(Topic)
admin.site.register(Comment)
admin.site.register(Category)
